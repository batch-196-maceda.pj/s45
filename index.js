console.log("hi");

console.log(document);
// result: document HTML code

const txtFirstName = document.querySelector("#txt-first-name");
//console.log(txtFirstName);
// result: input field / tag

const txtLastName = document.querySelector("#txt-last-name");
/*
	alternative ways:
		>> document.getElementById("txt-first-name");
		>> document.getElementByClassName("text-class");
		>> document.getElementByTagName("h1");
*/

// target the full name
let spanFullName = document.querySelector("#span-full-name");

// event listeners
// (event) can have a shorthand of (e)
// addEventListener('stringOfAnEvent', function)
// innerHTML allows us to call the tag and change the value dynamically
// Each HTML element has an innerHTML property that defines both the HTML code and the text that occurs between that element's opening and closing tag. By changing an element's innerHTML after some user interaction, you can make much more interactive pages.

// txtFirstName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = txtFirstName.value;
// });
//secondary solution
// txtLastName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = txtFirstName.value+txtLastName.value;
// });

//txtLastName.addEventListener('keyup', (event) => {
//	spanFullName.innerHTML = txtFirstName.value+txtLastName.value;
//});

//txtFirstName.addEventListener('keyup', (event) => {
//	console.log(event);
//	console.log(event.target);
//});

//strech

// const keyCode =(e)=>{
// 	let kc = e.keyCode;
// 	if (kc === 65){
// 		e.target.value = null;
// 		alert("Someone clicked a");
// 	}
// }

//txtFirstName.addEventListener('keyup', keyCode);

//Activity solutions!
// let firstName = (e)=>{
// 	let fName = e.firstName;
// 	fName.innerHTML = txtFirstName.value;}

// firstName.addEventListener('keyup', firstName);

// let lastName = (e)=>{
// 	let lName = e.lastName;

// 	lName.innerHTML = txtLastName.value;}

// lName.addEventListener('keyup', lName);


// const txtLastName = document.querySelector("#txt-last-name");
// const txtFirstName = document.querySelector("#txt-first-name");

const full_Name=(e)=>{
	spanFullName.innerHTML = txtFirstName.value+" "+txtLastName.value;
}


txtFirstName.addEventListener('keyup',full_Name);
txtLastName.addEventListener('keyup',full_Name);